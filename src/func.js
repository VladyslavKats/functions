const getSum = (str1, str2) => {
  if(typeof str1 !== 'string' || typeof str2 !== 'string'){
    return false;
  }
  if(!/^\d+$/.test(str1) && !/^\d+$/.test(str2)){
    return false;
  }
  return (+str1 + +str2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;
  for (const post of listOfPosts) {
    if(post.author === authorName){
      posts++;
    }
    if('comments' in post){
      for (const comment of post.comments) {
        if(comment.author === authorName){
          comments++;
        }
      }
    }
    
  }
  return `Post:${posts},comments:${comments}`;
};

const tickets=(people)=> {
  let wallet = 0;
  for (let index = 0; index < people.length; index++) {
    if(index === 0 && (+people[0]) !== 25){
      return 'NO';
    }else if((+people[index]) / 25 === 1){
      wallet += +people[index];
    }else if((+people[index]) % 25 === 0 && ((+people[index]) - 25) <= wallet){
      wallet += ((+people[index]) - 25);
    }else{
      return 'NO';
    }
    console.log(wallet)
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
